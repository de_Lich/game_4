﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class PlayerController : MonoBehaviour
{
    [Header("General")]
    [Tooltip("In ms^-1")] [SerializeField] float speed = 20f;
    [Tooltip("In m")] [SerializeField] float xRange = 7f;
    [Tooltip("In m")] [SerializeField] float yRange = 5f;
    [SerializeField] GameObject[] guns;
    [Header("Control")]
    [SerializeField] float posPitchFactor = -2.5f;
    [SerializeField] float posYawFactor = 1f;
    [SerializeField] float posRollFactor = 2f;
    [SerializeField] float controlPitchFactor = -15f;

    AudioSource audiosource;
    [SerializeField] AudioClip laserShot;


    float xThrow, yThrow;
    public bool isControlEnabled = true;


    void Start()
    {
        audiosource = GetComponent<AudioSource>();
    }
    void OnPlayerDeath()
    {
        isControlEnabled = false;
    }

    void Update()
    {
        if (isControlEnabled)
        {
            ProcessTranslation();
            ProcessRotation();
            ProcessFiring();
        }
    }

    void ProcessRotation()
    {
        float pitch = (transform.localPosition.y * posPitchFactor) + (yThrow * controlPitchFactor);        // x for Quaternion
        float yaw = transform.localPosition.x * posYawFactor;         // y for Quaternion
        float roll = transform.localPosition.z * posRollFactor * -xThrow;        // z for Quaternion
        transform.localRotation = Quaternion.Euler(pitch, yaw, roll);
    }

    void ProcessTranslation()
    {
        xThrow = CrossPlatformInputManager.GetAxis("Horizontal");//доступ к горизонтальной оси
        float xOffset = xThrow * speed * Time.deltaTime;//скорость перемещения по оси Х
        float rawXPos = transform.localPosition.x + xOffset;// точка отсчета для перемещения;
        float clampedXPos = Mathf.Clamp(rawXPos, -xRange, xRange);//диапазон горизонтальный

        yThrow = CrossPlatformInputManager.GetAxis("Vertical");//доступ к  оси Y
        float yOffset = yThrow * speed * Time.deltaTime;//скорость перемещения по оси Y
        float rawYPos = transform.localPosition.y + yOffset;// точка отсчета для перемещения;
        float clampedYPos = Mathf.Clamp(rawYPos, -yRange, yRange);//диапазон вертикальный

        transform.localPosition = new Vector3(clampedXPos, clampedYPos, transform.localPosition.z);
    }
    void ProcessFiring()
    {
        if (CrossPlatformInputManager.GetButton("Fire"))
        {
            SetGunsActive(true);
            if (!audiosource.isPlaying)
            {
                audiosource.PlayOneShot(laserShot);
            }
        }
        else
        {
            SetGunsActive(false);
            audiosource.Stop();
        }
    }

    public void SetGunsActive(bool isActive)
    {
        foreach (GameObject gun in guns)
        {
            var emissionMod = gun.GetComponent<ParticleSystem>().emission;
            emissionMod.enabled = isActive;
        }
    }
}
