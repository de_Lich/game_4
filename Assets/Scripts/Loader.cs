﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Loader : MonoBehaviour
{
    [SerializeField] GameObject backToMenu;

    public void Start()
    {
        if(SceneManager.GetActiveScene().name == "Splash")
        {
            Cursor.visible = true;
        }
        backToMenu.SetActive(false);
        Cursor.visible = false;
    }
    public void Update()
    {
        if(SceneManager.GetActiveScene().name != "Splash")
        {
            Cursor.visible = false;
        }
        Invoke("ShowWinCanvas", 138f);
    }
    public void LoadGame()
    {
        SceneManager.LoadScene(1);
    }
    public void QuitGame()
    {
        Application.Quit();
    }
    public void ToMenu()
    {
        SceneManager.LoadScene(0);
    }
    public void ShowWinCanvas()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        backToMenu.SetActive(true);
        FindObjectOfType<PlayerController>().SetGunsActive(false);
        FindObjectOfType<PlayerController>().isControlEnabled = false;
    }
}
