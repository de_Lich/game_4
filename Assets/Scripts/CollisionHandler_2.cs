﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CollisionHandler_2 : MonoBehaviour
{
    [SerializeField] float levelLoadDelay = 2f;
    [SerializeField] GameObject deathFX;
    [SerializeField] GameObject destrobleShip;
    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Friendly")) return; // to do
        gameObject.GetComponent<MeshRenderer>().enabled = false;
        FindObjectOfType<PlayerController>().SetGunsActive(false);
        //Instantiate(destrobleShip, transform.position, Quaternion.identity);
        StartDeathSequence();
        deathFX.SetActive(true);
        Invoke("ReloadScene", levelLoadDelay);
    }

    void StartDeathSequence()
    {
        SendMessage("OnPlayerDeath");
    }
    private void ReloadScene()
    {
        SceneManager.LoadScene(0);
    }
}
